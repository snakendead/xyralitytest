//
//  ResultsViewModel.h
//  xyrality
//
//  Created by developer2 on 06.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewModelServiceProtocol.h"

@interface ResultsViewModel : NSObject

@property (nonatomic) id<ViewModelServiceProtocol> viewModelService;
@property (nonatomic, readonly) NSArray<WorldEntity *> *worlds;

- (instancetype)initWithViewModelServices:(id<ViewModelServiceProtocol>)viewModelService worlds:(NSArray<WorldEntity *> *)worlds;

@end
