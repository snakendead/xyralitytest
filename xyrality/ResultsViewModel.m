//
//  ResultsViewModel.m
//  xyrality
//
//  Created by developer2 on 06.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

#import "ResultsViewModel.h"

@interface ResultsViewModel()

@property (nonatomic) NSArray<WorldEntity *> *worlds;

@end

@implementation ResultsViewModel

- (instancetype)initWithViewModelServices:(id<ViewModelServiceProtocol>)viewModelService worlds:(NSArray<WorldEntity *> *)worlds {
    self = [super init];
    if (self) {
        self.viewModelService = viewModelService;
        self.worlds = worlds;
    }
    return self;
}

@end
