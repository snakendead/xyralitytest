//
//  WorldStatusEntity+CoreDataProperties.m
//  xyrality
//
//  Created by developer2 on 06.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WorldStatusEntity+CoreDataProperties.h"

@implementation WorldStatusEntity (CoreDataProperties)

@dynamic description_;
@dynamic id;

@end
