//
//  WorldEntity+CoreDataProperties.h
//  xyrality
//
//  Created by developer2 on 06.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WorldEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface WorldEntity (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *country;
@property (nullable, nonatomic, retain) NSString *id;
@property (nullable, nonatomic, retain) NSString *language;
@property (nullable, nonatomic, retain) NSString *mapURL;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *url;
@property (nullable, nonatomic, retain) WorldEntity *worldStatus;

@end

NS_ASSUME_NONNULL_END
