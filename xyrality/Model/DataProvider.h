//
//  DataProvider.h
//  xyrality
//
//  Created by developer2 on 06.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "WorldStatusEntity.h"
#import "WorldEntity.h"
#import "UserEntity.h"
#import <MagicalRecord/MagicalRecord.h>
#import <CoreData/CoreData.h>

@interface DataProvider : NSObject

- (RACSignal *)findWorldsSignalWithUser:(UserEntity *)user;

@end

@interface NSManagedObject(Parsing)

- (NSDictionary *)toJSON;
- (void)safeSetValuesForKeysWithDictionary:(NSDictionary<NSString *, id> *)keyedValues;

@end
