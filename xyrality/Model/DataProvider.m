//
//  DataProvider.m
//  xyrality
//
//  Created by developer2 on 06.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

#import "DataProvider.h"

@implementation DataProvider

typedef NS_ENUM(NSUInteger, ActionType) {
    ActionTypeGet,
    ActionTypePost,
    ActionTypeDelete,
    ActionTypePut
};

- (RACSignal *)findWorldsSignalWithUser:(UserEntity *)user {
    NSURL *url = [NSURL URLWithString:@"http://backend1.lordsandknights.com/XYRALITY/WebObjects/BKLoginServer.woa/wa/worlds"];
    NSString *deviceType = [NSString stringWithFormat:@"%@ - %@ %@", [[UIDevice currentDevice] model], [[UIDevice currentDevice] systemName], [[UIDevice currentDevice] systemVersion]];
    NSString *deviceId = [[NSUUID UUID] UUIDString];
    NSDictionary *params = @{@"login": user.username,
                             @"password": user.password,
                             @"deviceType": deviceType,
                             @"deviceId": deviceId};
    
    return [[self requestSignalWithURL:url parameters:params actionType:ActionTypeGet successCodes:@[]] map:^NSArray *(NSDictionary *value) {
        NSArray *array = value[@"allAvailableWorlds"];
        NSMutableArray *parsedArray = [NSMutableArray new];
        for (NSDictionary *item in array) {
            WorldEntity *entity = [WorldEntity MR_createEntity];
            [entity safeSetValuesForKeysWithDictionary:item];
            [parsedArray addObject:entity];
        }
        return parsedArray;
    }];
}

- (RACSignal *)requestSignalWithURL:(NSURL *)url parameters:(NSDictionary<NSString *, id> *)params actionType:(ActionType)type successCodes:(NSArray<NSNumber *> *)array {
    
    NSArray *successCodes = [array arrayByAddingObjectsFromArray:@[@200, @201, @202]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:(NSURLRequestReloadIgnoringLocalAndRemoteCacheData) timeoutInterval:10.0];
    
    NSDictionary<NSNumber *, NSString *> *methodMap = [[NSDictionary alloc] initWithObjects:@[@"GET", @"POST", @"DELETE", @"PUT"] forKeys:@[@(ActionTypeGet), @(ActionTypePost), @(ActionTypeDelete), @(ActionTypePut)]];
    
    request.HTTPMethod = methodMap[@(type)];
    
    if (params.count > 0) {
        if (type != ActionTypeGet) {
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            NSData *body = [NSJSONSerialization dataWithJSONObject:params options:(NSJSONWritingPrettyPrinted) error:nil];
            request.HTTPBody = body;
        } else {
            NSURL *encodedURL = [self processURLForEncodingWithURL:url andParams:params];
            [request setURL:encodedURL];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        }
    }
    
    
    return [[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                             completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                                                                                                          
                                                 if (error) {
                                                     [subscriber sendError:error];
                                                 } else {
                                                     if ([successCodes containsObject:[NSNumber numberWithInteger:(((NSHTTPURLResponse *)response).statusCode)]]) {
                                                     
                                                         NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];

                                                         if (data) {
                                                             NSPropertyListFormat *format = nil;
                                                             NSError *tempError = [NSError errorWithDomain:@"" code:0 userInfo:nil];
                                                             NSDictionary* plist = [NSPropertyListSerialization propertyListWithData:data options:(NSPropertyListImmutable) format:format error:&tempError];

                                                             if (!tempError) {
                                                                 [subscriber sendNext:plist];
                                                                 [subscriber sendCompleted];
                                                             } else {
                                                                 [subscriber sendError:tempError];
                                                             }
                                                         } else {
                                                             [subscriber sendError:error];
                                                         }
                                                     } else {
                                                         [subscriber sendError:[NSError errorWithDomain:@"Wrong status code" code:((NSHTTPURLResponse *)response).statusCode userInfo:nil]];
                                                     }
                                                 }
                                      
                                             }];
        [task resume];
        return [RACDisposable disposableWithBlock:^{
            [task cancel];
        }];
    }] deliverOn:[RACScheduler mainThreadScheduler]];
}


- (NSURL *)processURLForEncodingWithURL:(NSURL *)url andParams:(NSDictionary<NSString *, NSString *> *)params {
    
    NSURLComponents *cmps = [NSURLComponents componentsWithURL:url resolvingAgainstBaseURL:false];
    NSMutableArray *queryItems = [NSMutableArray array];
    [params enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, NSString *  _Nonnull obj, BOOL * _Nonnull stop) {
        NSURLQueryItem *item = [NSURLQueryItem queryItemWithName:key value:obj];
        [queryItems addObject:item];
    }];
    [cmps setQueryItems:queryItems];
    return [cmps URL];
}

@end

@implementation NSManagedObject(Parsing)

- (NSDictionary *)toJSON {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    NSDictionary<NSString *, NSAttributeDescription *> *attributes = [self.entity attributesByName];
    [attributes enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, NSAttributeDescription * _Nonnull obj, BOOL * _Nonnull stop) {
        id value = [self valueForKey:key];
        if ([value isKindOfClass:[NSManagedObject class]]) {
            dict[key] = [(NSManagedObject *)value toJSON];
        } else {
            dict[key] = value;
        }
    }];
    
    return dict;
}

- (void)safeSetValuesForKeysWithDictionary:(NSDictionary<NSString *, id> *)keyedValues {
    NSDictionary<NSString *, NSAttributeDescription *> *attributes = [self.entity attributesByName];
    [attributes enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, NSAttributeDescription * _Nonnull obj, BOOL * _Nonnull stop) {
        id newValue = nil;
        if ([key isEqualToString:@"description_"]) {
            newValue = keyedValues[@"description"];
        } else {
            newValue = keyedValues[key];
        }
        
        if (newValue) {
            NSAttributeType type = obj.attributeType;
            if (type == NSStringAttributeType) {
                if (![newValue isKindOfClass:[NSString class]]) {
                    if ([newValue respondsToSelector:(NSSelectorFromString(@"string"))] ) {
                        newValue = [newValue string];
                    }
                }
            }
            [self setValue:newValue forKey:key];
        }
    }];
    
    NSDictionary<NSString *, NSRelationshipDescription *> *relationships = [self.entity relationshipsByName];
    [relationships enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, NSRelationshipDescription * _Nonnull obj, BOOL * _Nonnull stop) {
        id newValue = keyedValues[key];
        if (newValue) {
            NSDictionary<NSString *,NSManagedObject *> *map = [[NSDictionary alloc]initWithObjectsAndKeys:
                                                               [WorldStatusEntity MR_createEntity], @"worldStatus",
                                                               nil];
            
            NSManagedObject *object = map[key];
            if (obj.toMany) {
                NSMutableSet<NSManagedObject *> *set = [[NSMutableSet alloc] init];
                NSArray<NSDictionary<NSString *, id> *> *array = (NSArray<NSDictionary<NSString *, id> *> *)newValue;
                
                [array enumerateObjectsUsingBlock:^(NSDictionary<NSString *,id> * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    [object safeSetValuesForKeysWithDictionary:obj];
                    [set addObject:object];
                }];
                newValue = set;
            } else {
                [object safeSetValuesForKeysWithDictionary:(NSDictionary<NSString *, id> *)newValue];
                newValue = object;
            }
            
            [self setValue:newValue forKey:key];
        }
    }];
}

@end
