//
//  WorldStatusEntity+CoreDataProperties.h
//  xyrality
//
//  Created by developer2 on 06.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WorldStatusEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface WorldStatusEntity (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *description_;
@property (nullable, nonatomic, retain) NSString *id;

@end

NS_ASSUME_NONNULL_END
