//
//  TableViewBindingHelper.m
//  xyrality
//
//  Created by developer2 on 06.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

#import "TableViewBindingHelper.h"
#import "WorldEntity.h"
#import "WorldStatusEntity.h"

@implementation ResultTableViewCell

@end

@interface TableViewBindingHelper() <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) UITableView *tableView;
@property (nonatomic) UITableViewCell *templateCell;
@property (nonatomic) NSArray *data;

@end

@implementation TableViewBindingHelper

- (instancetype)initWithTableView:(UITableView *)tableView templateCellNib:(UINib *)templateCellNib dataObserve:(RACSignal *)dataSignal
{
    self = [super init];
    if (self) {
        self.tableView = tableView;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.scrollEnabled = true;
        self.tableView.userInteractionEnabled = true;
        self.templateCell = [[templateCellNib instantiateWithOwner:nil options:nil] firstObject];
        [self.tableView registerNib:templateCellNib forCellReuseIdentifier:self.templateCell.reuseIdentifier];
        [self observeData:dataSignal];
    }
    return self;
}

- (void)observeData:(RACSignal *)signal {
    @weakify(self)
    [signal subscribeNext:^(id next) {
        
        @strongify(self)
        if ([next isKindOfClass:[NSArray class]]) {
            self.data = next;
        }
        
        [self.tableView reloadData];
    }];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:_templateCell.reuseIdentifier forIndexPath:indexPath];
    if ([_templateCell.reuseIdentifier isEqualToString:@"ResultTableViewCell"]) {
        [self processResultCell:(ResultTableViewCell *)cell forIndexPath:indexPath];
    } else {
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.selectionCommand) {
        [self.selectionCommand execute:self.data[indexPath.row]];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}

- (void)processResultCell:(ResultTableViewCell *)cell forIndexPath:(NSIndexPath *)indexPath {
    id item = self.data[indexPath.row];
    if ([item isKindOfClass:[WorldEntity class]]) {
        WorldEntity *world = (WorldEntity *)item;
        NSString *imagePath = [NSString stringWithFormat:@"CountryPicker.bundle/%@", world.country.uppercaseString];
        UIImage *image = [UIImage imageNamed:imagePath];
        cell.flagImageView.image = image;
        cell.nameLabel.text = world.name;
        
        NSDictionary *map = @{@"1": [UIColor redColor],
                              @"2": [UIColor yellowColor],
                              @"3": [UIColor greenColor]};
        
        cell.statusView.layer.backgroundColor = [map[world.worldStatus.id] CGColor];
    }
}

@end
