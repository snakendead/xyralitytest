//
//  ViewController.m
//  xyrality
//
//  Created by developer2 on 06.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

#import "LoginViewController.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "ResultsViewModel.h"

@interface LoginViewController ()

@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *findButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;

@property (nonatomic) LoginViewModel *viewModel;

@end

@implementation LoginViewController

- (instancetype)initWithViewModel:(LoginViewModel *)viewModel {
    self = [super initWithNibName:@"LoginViewController" bundle:nil];
    if (self) {
        self.viewModel = viewModel;
        [self firstInitialize];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self bindViewModel];
    [self bindActions];
}

- (void)firstInitialize {
    
}

- (void)bindViewModel {
    
    @weakify(self)
    
    RACSignal *usernameSignal =
        self.usernameTextField.rac_textSignal;

    RACSignal *passwordSignal =
        self.passwordTextField.rac_textSignal;
    
    RACSignal *mergedTextSignal = [RACSignal combineLatest:@[usernameSignal, passwordSignal]];
    
    [mergedTextSignal subscribeNext:^(NSNumber *valid) {
        @strongify(self)
       [self checkFindButtonEnableState];
    }];
}

- (void)bindActions {
    
    @weakify(self)
    RACSignal *showSignal = [[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillShowNotification object:nil];
    RACSignal *hideSignal = [[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillHideNotification object:nil];
    RACSignal *merged = [RACSignal combineLatest:@[showSignal, hideSignal]];
    [merged subscribeNext:^(RACTuple *tuple) {
        @strongify(self)
        NSNotification *notification = [tuple first];
        NSValue *rectValue = notification.userInfo[UIKeyboardFrameEndUserInfoKey];
        NSNumber *doubleValue = notification.userInfo[UIKeyboardAnimationDurationUserInfoKey];
        [self animateConstraintWithOrigin:rectValue.CGRectValue.origin.y duration:doubleValue.doubleValue];
    }];
    
    [[[[self.findButton rac_signalForControlEvents:(UIControlEventTouchUpInside)]
     doNext:^(id x) {
         @strongify(self)
         self.findButton.enabled = false;
         [self.activityIndicator setHidden:false];
     }] flattenMap:^RACStream *(id value) {
         @strongify(self)
         return [self getWorldsSignal];
     }] subscribeNext:^(NSArray *entities) {
         @strongify(self)
         self.findButton.enabled = true;
         [self.activityIndicator setHidden:true];
         ResultsViewModel *viewModel = [[ResultsViewModel alloc] initWithViewModelServices:self.viewModel.viewModelService worlds:entities];
         [self.viewModel.viewModelService pushViewModel:viewModel];
     } error:^(NSError *error) {
         @strongify(self)
         self.findButton.enabled = true;
         [self.activityIndicator setHidden:true];
         UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Server error" preferredStyle:(UIAlertControllerStyleAlert)];
         UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:(UIAlertActionStyleCancel) handler:nil];
         [alert addAction:action];
         [self presentViewController:alert animated:true completion:nil];
     }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self fillPage];
}

- (void)fillPage {
    self.title = @"Sign in";
    self.usernameTextField.text = @"ios.test@xyrality.com";
    self.passwordTextField.text = @"password";
    [self checkFindButtonEnableState];
}
     
- (void)checkFindButtonEnableState {
     self.findButton.enabled = [self isValidUsername:self.usernameTextField.text] && [self isValidPassword:self.passwordTextField.text];
}

- (BOOL)isValidUsername:(NSString *)username {
    return username.length > 2 && [username containsString:@"@"];
}

- (BOOL)isValidPassword:(NSString *)password {
    return password.length > 4;
}
     

- (void)animateConstraintWithOrigin:(CGFloat)origin duration:(double)duration {
    self.bottomConstraint.constant = self.view.frame.size.height - origin;
    [self finishConstraintAnimation:duration];
}

- (void)finishConstraintAnimation:(double)duration {
    [UIView animateWithDuration:duration animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (RACSignal *)getWorldsSignal {
    return [self.viewModel getWorldsWithUsername:self.usernameTextField.text andPassword:self.passwordTextField.text];
}

@end
