//
//  ViewController.h
//  xyrality
//
//  Created by developer2 on 06.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewModel.h"

@interface LoginViewController : UIViewController

- (instancetype)initWithViewModel:(LoginViewModel *)viewModel;

@end

