//
//  ResultsViewController.m
//  xyrality
//
//  Created by developer2 on 06.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

#import "ResultsViewController.h"
#import "TableViewBindingHelper.h"

@interface ResultsViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) TableViewBindingHelper *bindingHelper;
@property (nonatomic) UIBarButtonItem *refreshItem;
@property (nonatomic) ResultsViewModel *viewModel;

@end

@implementation ResultsViewController

- (instancetype)initWithViewModel:(ResultsViewModel *)viewModel {
    self = [super initWithNibName:@"ResultsViewController" bundle:nil];
    if (self) {
        self.viewModel = viewModel;
        [self firstInitialize];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self bindViewModel];
    [self bindActions];
}

- (void)firstInitialize {
    self.tableView.bounces = YES;
}

- (void)bindViewModel {
    UINib *nib = [UINib nibWithNibName:@"ResultTableViewCell" bundle:nil];
    RACSignal *observe = RACObserve(self.viewModel, worlds);
    self.bindingHelper = [[TableViewBindingHelper alloc] initWithTableView:self.tableView templateCellNib:nib dataObserve:observe];
}

- (void)bindActions {
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self fillPage];
}

- (void)fillPage {
    self.title = @"Results";
}

@end
