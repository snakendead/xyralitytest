//
//  LoginViewModel.m
//  xyrality
//
//  Created by developer2 on 06.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

#import "LoginViewModel.h"

@interface LoginViewModel ()

@end

@implementation LoginViewModel

- (instancetype)initWithViewModelServices:(id<ViewModelServiceProtocol>)viewModelService
{
    self = [super init];
    if (self) {
        self.viewModelService = viewModelService;
    }
    return self;
}

- (RACSignal *)getWorldsWithUsername:(NSString *)username andPassword:(NSString *)password {
    UserEntity *user = [UserEntity MR_createEntity];
    [user setUsername:username];
    [user setPassword:password];
    return [self.viewModelService.dataProvider findWorldsSignalWithUser:user];
}

@end
