//
//  LoginViewModel.h
//  xyrality
//
//  Created by developer2 on 06.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewModelServiceProtocol.h"

@interface LoginViewModel : NSObject

@property (nonatomic) id<ViewModelServiceProtocol> viewModelService;

- (instancetype)initWithViewModelServices:(id<ViewModelServiceProtocol>)viewModelService;

- (RACSignal *)getWorldsWithUsername:(NSString *)username andPassword:(NSString *)password;

@end
