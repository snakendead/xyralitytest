//
//  ViewModelService.m
//  xyrality
//
//  Created by developer2 on 06.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

#import "ViewModelService.h"
#import "LoginViewController.h"
#import "ResultsViewController.h"

@interface ViewModelService()

@property (nonatomic) UINavigationController *navigationController;
@property (nonatomic) DataProvider *dataProvider;

@end

@implementation ViewModelService

-(instancetype) init {
    self = [super init];
    if (self) {
        self.dataProvider = [[DataProvider alloc] init];
    }
    return self;
}

-(void)setRootViewController:(UIViewController *)viewController {
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self switchRootController:self.navigationController];
    
}

-(void)pushViewModel:(id)viewModel {
    id viewController = nil;
    
    if ([viewModel isKindOfClass:LoginViewModel.class]) {
        viewController = [[LoginViewController alloc] initWithViewModel:viewModel];
    } else if ([viewModel isKindOfClass:ResultsViewModel.class]) {
        viewController = [[ResultsViewController alloc] initWithViewModel:viewModel];
    }
    [self.navigationController pushViewController:viewController animated:true];
}

-(void)switchRootController:(UIViewController *)viewController {
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    [UIView transitionWithView:window
                      duration:0.33
                       options:(UIViewAnimationOptionTransitionCrossDissolve)
                    animations:^{
                        window.rootViewController = viewController;
                    }
                    completion:^(BOOL finished) {
                        
                    }];
}

@end
