//
//  ViewModelService.h
//  xyrality
//
//  Created by developer2 on 06.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewModelServiceProtocol.h"

@interface ViewModelService : NSObject <ViewModelServiceProtocol>

@property (nonatomic, readonly) DataProvider *dataProvider;

@end
