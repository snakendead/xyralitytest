//
//  ViewModelServiceProtocol.h
//  xyrality
//
//  Created by developer2 on 06.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "DataProvider.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@protocol ViewModelServiceProtocol <NSObject>

@property (nonatomic, readonly) DataProvider *dataProvider;

-(void)pushViewModel:(id)viewModel;
-(void)setRootViewController:(UIViewController *)viewController;

@end
