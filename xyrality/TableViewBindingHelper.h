//
//  TableViewBindingHelper.h
//  xyrality
//
//  Created by developer2 on 06.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface ResultTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *flagImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIView *statusView;

@end

@interface TableViewBindingHelper : NSObject

@property (nonatomic) RACCommand *selectionCommand;

- (instancetype)initWithTableView:(UITableView *)tableView templateCellNib:(UINib *)templateCellNib dataObserve:(RACSignal *)dataSignal;

@end
