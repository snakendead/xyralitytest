//
//  WorldEntity.h
//  xyrality
//
//  Created by developer2 on 06.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class WorldStatusEntity;

NS_ASSUME_NONNULL_BEGIN

@interface WorldEntity : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "WorldEntity+CoreDataProperties.h"
